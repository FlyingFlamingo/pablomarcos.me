---
title: Graph and networks methods for metabolomics data analysis and interpretation
outputs: Reveal
reveal_hugo:
  margin: 0.2
  width: 1200
  height: 675
  slide_number: true
  highlight_theme: color-brewer
  transition: slide
  transition_speed: medium
  templates:
    hotpink:
      class: hotpink
      background: '#FF4081'

---

<!-- Remove the ugly squares the default theme applies -->
<style>
.reveal section img {
  margin: 0px;
    margin-right: 0px;
  background: none;
  border: none;
  box-shadow: none
}
</style>

{{< slide background-image="/reveal-themes/iarc-template/square-logo-2.jpg" >}}

</br></br>

### Graph and networks methods
#### for metabolomics data analysis and interpretation

~ by [Pablo Ignacio Marcos López](https://www.pablomarcos.me/) \
and [Adam Amara]()

</br></br>

<div style="display: flex" class="row">
  <div style="flex: 50%" class="column ">
    <img style="margin-right: 30%;" data-src="IARC Logo.png" width="65%">
  </div>
  <div style="flex: 50%" class="column ">
  </div>
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

<h3 style="text-align:left"> Introduction</h3>

* Metabolomics data is abundant, and publications are growing exponentially

<img class="fragment" data-src="pubmed-metabolmics.png" height="200px" width="75%">

* It can be difficult to make sense of all this different data, and to find relevant information across different databases

* Sometimes, information is incomplete, synonyms are not taken into account, and the results are not homogeneous

---


{{< slide background-image="/reveal-themes/iarc-template/circles-blue.jpg"
          notes="This is a notes test">}}

* To solve this, we created [**CanGraph**](https://omb-iarc.github.io/CanGraph/), a python utility:
![](flowchart-full.jpg)

---

{{% section %}}
{{< slide background-image="/reveal-themes/iarc-template/square-logo-1.jpg" >}}

### 💡 But, how does it work?

---

{{< slide background-image="/reveal-themes/iarc-template/circles-blue.jpg" >}}

#### It searches for information in five top-quality databases


<div class="r-stack">
  <img class="fragment" src="hmdb.png" width="30%">
  <img class="fragment" src="drugbank.png" width="30%">
  <img class="fragment" src="smpdb.png" width="30%">
  <img class="fragment" src="exposome-explorer.jpg" width="30%">
  <img class="fragment" src="wikidata.png" width="30%"">
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/circles-blue-inverted.jpg" >}}

##### while solving the synonym problem using

<div class="r-stack">
  <img class="fragment" src="metanetx.jpeg" width="30%">
  <img class="fragment" src="cts.png" width="30%">
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/circles-blue.jpg" >}}

##### and offering two running modes

* **A faster, indexed mode** where only valid identifiers are selected for import

* **A slower, text-search mode** in which the text is directly queried for info

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

##### and six search parameters:

</br></br>
<div style="display: flex" class="row">
  <div style="flex: 50%" class="column fragment">
    HMDB ID
  </div>
  <div style="flex: 50%" class="column fragment">
    CHEBI ID
  </div>
</div>
</br>
<div style="display: flex" class="row">
  <div style="flex: 50%" class="column fragment">
    Name
  </div>
  <div style="flex: 50%" class="column fragment">
    MeSH ID
  </div>
</div>
</br>
<div style="display: flex" class="row">
  <div style="flex: 50%" class="column fragment">
    InChI
  </div>
  <div style="flex: 50%" class="column fragment">
    InChIKey
  </div>
</div>

{{% /section %}}

---

{{< slide background-image="/reveal-themes/iarc-template/circles-blue.jpg" >}}

* So, to recap:
![](flowchart-full.jpg)

---

{{< slide background-image="/reveal-themes/iarc-template/square-logo-6.jpg" >}}

## 🧐 Now, lets see some examples!

---

{{< slide background-image="/reveal-themes/iarc-template/shape-white-logo.jpg" >}}
![](figure_2.png)
First, lets check the **Schema**

---

{{% section %}}
{{< slide background-image="/reveal-themes/iarc-template/shape-big-blue.jpg" >}}

For the *first* example, we picked <h5> **1-Amino-2-propanol** </h5>

</br>

* An amino-alcohol involved in the biosynthesis of cobalamin
* From 3 identifiers → 6 synonyms
* Appeared during lab research

<div style="display: flex;" width="90%">
  <div style="flex: 40%; padding: 5px;">
    </br>
    <img data-src="1-amino-2-propanol-formula.png" width="45%">
  </div>
  <div style="flex: 40%; padding: 5px;">
    <img data-src="1-amino-2-propanol-ballsnsticks.png" width="45%">
  </div>
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

<div style="display: flex;" width="90%">
  <div style="flex: 20%">
    <img data-src="aminopropanol-noindex-summary.png" width="100%">
    <small>Without Index</small>
  </div>
  <div style="flex: 20%">
    <img data-src="aminopropanol-index-summary.png" width="100%">
    <small>With Index</small>
  </div>
    <div style="flex: 50%">
       <ul>
        <li>As expected, when we use the index we get less information, but presumably of higher quality (only real matches, no noise)</li>
        </br></br>
        <li>For the rest of the example, we will use the indexed version</li>
      </ul>
  </div>
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-big-blue.jpg" >}}
![](references-aminopropanol.png)
We can query the database for a list of references related to our metabolite of interest, together with their PubMed IDs

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}
We can also find out taxonomical categories and body location
</br></br>
<img data-src="related-nodes-aminopropanol.png" width="65%">

{{% /section %}}

---

{{% section %}}
{{< slide background-image="/reveal-themes/iarc-template/shape-white-logo.jpg" >}}

For *one last* example, we picked <h5> **Hepatocellular Carcinoma** </h5>

</br>

* The most common type of primary liver cancer
* The third leading cause of cancer-related deaths
* Not a metabolite → we will need to adapt

<div style="display: flex;" width="70%">
  <div style="flex: 80%">
    </br>
    <img data-src="hepatocellular-carcinoma.jpg" width="45%">
  </div>
  <div style="flex: 40%">
    </br></br></br>
    <small><a href="https://commons.wikimedia.org/wiki/File:Hepatocellular_carcinoma_intermed_mag.jpg">Micrograph of hepatocellular carcinoma.</a> <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.en">CC-By-SA 3.0</a> <a href="https://commons.wikimedia.org/wiki/User:Nephron">by Nephron</a></small>
  </div>
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/circles-blue.jpg" >}}

##### 🥇 Approach: Direct Search

<div style="display: flex;" width="90%">
    <div style="flex: 50%">
       </br></br>
       <ul>
        <li>We directly searched for HCC with the <em>--noindex</em> flag</li>
        <li>Only a small number of metabolites were imported</li>
        <li>The results were biased towards DrugBank and not really usefull → another approach is necessary</li>
      </ul>
    </div>
    <div style="flex: 15%">
      <img data-src="hcc-old-nodes.png" width="80%">
    </div>
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-big-blue.jpg" >}}

##### 🥈 Approach: Search via metabolites

</br>

* To get a more representative result, we extracted 12 metabolites related to HCC from DOI: [10.1002/ijc.33885](https://doi.org/10.1002/ijc.33885)
* From 12 identifiers → 145 synonyms
* Around 20 hours to process due to huge number of matches

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

<div>
  <img data-src="hcc-metabolite-publication.png" width="60%">
</div>
<div>
  <img data-src="hcc-disease-cellularlocation.png" width="60%">
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

<div>
  <img data-src="hcc-pathway.png" width="60%">
</div>
<div>
  <img data-src="hcc-taxonomy.png" width="60%">
</div>

---

{{< slide background-image="/reveal-themes/iarc-template/shape-small-blue.jpg" >}}

![](hcc-nodes-mashup.png)

An example of 50 nodes related to *Glycolic Acid*

{{% /section %}}

---

{{< slide background-image="/reveal-themes/iarc-template/square-logo-4.jpg" >}}

</br></br>

# Thank you!

<small>Bibliography and additional information can be found in the [Thesis](https://www.pablomarcos.me/files/Memoria%20TFM%20FIRMADA.pdf) and in the [web documentation](https://omb-iarc.github.io/CanGraph/)</small>

[↩️](#) Back to the beggining

</br></br></br></br>

<img style="position: absolute; right: 1000px; bottom: 0px;" data-src="creativecommons.png" width="20%">
