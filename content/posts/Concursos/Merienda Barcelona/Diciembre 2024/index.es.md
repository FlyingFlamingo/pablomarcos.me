---
title: "Merienda Barcelona - Diciembre 2024"
date: 2024-12-01
menu:
  sidebar:
    name: Diciembre 2024
    identifier: mb_diciembre2024
    parent: meriendabcn
author:
  name: Merienda Barcelona
  image: /images/author/Logo Merienda Barcelona.png
---

Como parte de las experiencias de este mes, he diseñado una "ruta modelo", para que podáis ver en qué consiste un paseo de Merienda Barcelona.

Empezando en Sants Estació, subimos por Josep Tarradellas hasta Diagonal, atendiendo a una media de 12 usuarios semanales!

La verdad es que las rutas son una experiencia muy emotiva y divertida, a la par que gratificante y, en mi opinión, útil. Si quieres unirte: mándame un mail, o escríbeme en [@meriendabarcelona](https://masto.es/@meriendabarcelona) !

<iframe width="100%" height="700px" frameborder="0" allowfullscreen allow="geolocation" src="//framacarte.org/en/map/ruta-de-ejemplo-merienda-barcelona_199976?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true"></iframe>

## Semana del 01.12

La de hoy ha sido una de las semanas más largas del proyecto! Más gente que nunca, sobre unas 15 personas, fueron atendidas, y eso que los materiales 🥐 del 365 no fueron igual de abundantes que otras veces... tuve que suplementarlos con un bocadillo 🥙 casero, que complementé con 🍊 mandarinas y caldo de escudella! ☕

Aparte de un pequeño roce, fue una ruta encantadora, aunque a uno de los usuarios le habían quitado a su perra... 🐶 Espero que la recupere pronto 😠

{{< img src="/es/posts/concursos/merienda-barcelona/diciembre-2024/02.12.jpeg" width="70%" align="center" title="Selección de Bocadillos en el 365 donde los compro" >}}

...

(En Construcción)
