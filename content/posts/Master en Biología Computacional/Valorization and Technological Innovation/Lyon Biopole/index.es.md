---
title: "Lyon Biopole"
author: Pablo Marcos
date: 2022-04-19
math: true
menu:
  sidebar:
    name: Lyon Biopole
    identifier: lyon_biopole
    parent: valorization_innovation
    weight: 40
---


One of the tasks of the Valorization and Technological Innovation subject was presenting a world innovation hub to the class. Since I was doing my internship at IARC in Lyon, I decided to talk about Lyon's Biopole here:

{{< embed-pdf src="/es/posts/master-en-biología-computacional/valorization-and-technological-innovation/lyon-biopole/Lyon Biopole.pdf" >}}
