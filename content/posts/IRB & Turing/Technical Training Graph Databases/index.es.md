---
title: "Technical Training - Graph DBs"
author: Pablo Marcos
date: 2024-11-15
menu:
  sidebar:
    name: Graphs Training
    identifier: technical_training_graphs
    parent: irb_turing_more
    weight: 40
---

{{< badge href="./Technical Training Graph Databases - NoLogo.pdf" badge="codeberg" >}} </br>

En Noviembre de 2024, di una Presentación Técnica al Departamento de Data Science de [Biome Makers](https://biomemakers.com/), donde trabajo 🤓.

Como me siento muy orgulloso, y creo que me ha quedado muy informativa, he decidido subirla a la web. **Una vez he eliminado toda información confidencial**, la dejo aquí:

---

{{< embed-pdf src="./Technical Training Graph Databases - NoLogo.pdf" >}}

📸 de Cabecera: [Marcela Miranda](https://unsplash.com/es/@marcelamiranda) en [Unsplash](https://unsplash.com/es/fotos/un-primer-plano-de-una-flor-purpura-rodeada-de-otras-flores-W4M2TdyjoBs), [Unsplash License](https://unsplash.com/es/licencia)
