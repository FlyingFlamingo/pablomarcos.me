---
title: "BiomeMakers Excersise"
author: Pablo Marcos
date: 2023-11-16
math: true
menu:
  sidebar:
    name: BiomeMakers Exercise
    identifier: biomemakers_exercise
    parent: irb_turing_more
    weight: 40
---

{{< badge href="./Notebook.ipynb" badge="jupyter" >}} </br>

During the process of a candidature for a Bioinformatics position at [Biome Makers](https://biomemakers.com/es/inicio), I was assigned the following exercise. In keeping with the spirit of this blog, I have only released my solution, and not the assignment, in order to both protect BiomeMaker's copyright and share my proposed solution to an interesting challenge.

{{< alert type="success" >}} I finally ended up working here! :p {{< /alert >}}

---

We have been presented with a dataset containing a series of measurements of relative abundance for different organisms, using either their **16S ribosomal RNA sub-unit for bacteria** (since only prokaryotes and plastids produce this kind of sub-unit), or their **Internal Transcribed Spacer (ITS)** in the case of fungi.

For the purpouses of this exercise, we want only to retain only taxa that have a combination of high prevalence and high abundance, where the prevalence threshold is allowed to be lower for taxa with very high abundance and vice-versa. Thus, our filter can better be described using the equation:

$$ y = (1 - ax) + b $$

where:
* _y_ is the prevalence threshold, expressed as the fraction of samples where a taxon must be found
* _x_ is the mean relative abundance of a taxon
* _a_ is the parameter that relates abundance to prevalence
* _b_ is the minimum allowed prevalence, which is required to avoid returning rare but very abundant taxa

We can start by exploring the data a bit:


```python
import pandas as pd # Pandas is a python library for data analysis
```


```python
df = pd.read_csv("tech_exercise_abn.csv")
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>sample_code</th>
      <th>marker</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0</td>
      <td>18</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.342292</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>11</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.026036</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2</td>
      <td>6</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.019074</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3</td>
      <td>15</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.154723</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4</td>
      <td>9</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.017627</td>
    </tr>
  </tbody>
</table>
</div>



As we can see, there is a suspicious column, which was probably erroneously generated in a previous export.


```python
# We can check this:
def drop_col_if_extra(df, verbose = True):
    if list(df.iloc[:, 0]) == list(range(len(df))):
        if verbose: print("Removing exact duplicates...")
        # And, if necessary, drop the values
        new_df = df.drop("Unnamed: 0", axis=1)
        return new_df
    else:
        return df
df = pd.read_csv("tech_exercise_abn.csv")
df = drop_col_if_extra(df)
df.head() # And show that everything went OK
```

    Removing exact duplicates...





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample_code</th>
      <th>marker</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>18</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.342292</td>
    </tr>
    <tr>
      <th>1</th>
      <td>11</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.026036</td>
    </tr>
    <tr>
      <th>2</th>
      <td>6</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.019074</td>
    </tr>
    <tr>
      <th>3</th>
      <td>15</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.154723</td>
    </tr>
    <tr>
      <th>4</th>
      <td>9</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.017627</td>
    </tr>
  </tbody>
</table>
</div>



Now that we have explored the data, we can move on with the exercise.
Our objectives are the following:

### Implement a function using this equation, so it filters for any given a and b

To do so, we can define the following function:


```python
def filter_by_treshold(df, a, b):
    """
    A function that filers by a given *prevalence threshold*,
    calculating it using the mandatory parameters *a* (abundace-prevalence
    ratio) and *b* (minimum allowed prevalence), and the formula 𝑦=(1−𝑎𝑥)+𝑏

    Args:
    df (pd.DataFrame): a pandas dataframe containing an unfiltered database
    a (float): the parameter that relates abundance to prevalence
    b (float): the minimum allowed prevalence

    Returns:
        pd.DataFrame: A pandas dataframe with the designed items filtered out
    """
    new_df = df.copy()  # Create a copy to avoid modifying the original DataFrame
    # If everything is OK, we have to calculate the mean_relative_abundace
    for taxon_id in df["taxonomy_id"].unique():
        # For this, we sub-divide the df in as much sub-dfs as taxon_ids there are
        sub_df = df[df["taxonomy_id"] == taxon_id]
        number_of_samples = len(sub_df["sample_code"].unique())
        # We calculate the mean % relative abundance
        percent_relative_abundace = sum(sub_df["percentage"])/number_of_samples
        # And we divide it by 100 to get a proportion, instead of a %
        mean_relative_abundace = percent_relative_abundace/100

        # With this abundance, we calculate the treshold using our formula
        prevalence_treshold = (1 - a*mean_relative_abundace) + b

        # Now, we will filter the original df using the prevalence_treshold
        # First, we must calculate the prevalence by dividing the number of
        # positive samples over the total number of samplings made
        prevalence = number_of_samples / len(df["sample_code"].unique())

        # And now, we filter:
        # If the calculated treshold makes sense
        if prevalence_treshold >= 0: #and prevalence_treshold <= 1
            # If the treshold is bigger than the prevalence value
            if prevalence_treshold > prevalence:
                new_df.drop(new_df[new_df['taxonomy_id'] == taxon_id].index, inplace = True)

    return new_df
```

Which we can test with some values, using the fact that jupyter will auto-print the resulting df. We know that:
* The minimum acceptable prevalence can be between 1 and 1/19, since there are at most 19 samples.
* The relation between abundance and prevalence (*a*) is unknown, and will be explored in point 4.a


```python
# First, we try reading the file and doing a small pre-processing
try:
    df = pd.read_csv("tech_exercise_abn.csv", sep=",")
    df = drop_col_if_extra(df, verbose=False)
except:
    raise ValueError(f"File {filename} does not exist")
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample_code</th>
      <th>marker</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>18</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.342292</td>
    </tr>
    <tr>
      <th>1</th>
      <td>11</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.026036</td>
    </tr>
    <tr>
      <th>2</th>
      <td>6</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.019074</td>
    </tr>
    <tr>
      <th>3</th>
      <td>15</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.154723</td>
    </tr>
    <tr>
      <th>4</th>
      <td>9</td>
      <td>16S</td>
      <td>6625</td>
      <td>0.017627</td>
    </tr>
  </tbody>
</table>
</div>




```python
# With a value of b = 0.25 (lax) and a = 400:
new_df = filter_by_treshold(df, 400, 0.25)
new_df.describe(percentiles=[0.25])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample_code</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>2356.000000</td>
      <td>2356.000000</td>
      <td>2356.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>9.462649</td>
      <td>24054.972411</td>
      <td>0.896903</td>
    </tr>
    <tr>
      <th>std</th>
      <td>5.541571</td>
      <td>14628.002089</td>
      <td>2.202330</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>6625.000000</td>
      <td>0.001188</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>5.000000</td>
      <td>8687.000000</td>
      <td>0.093691</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>10.000000</td>
      <td>34980.000000</td>
      <td>0.286815</td>
    </tr>
    <tr>
      <th>max</th>
      <td>18.000000</td>
      <td>40189.000000</td>
      <td>47.696011</td>
    </tr>
  </tbody>
</table>
</div>

We see some filtering is occuring; now, there are only 2356 rows, instead of the 6817 we started with With a value of b = 0.9 (strict) and a = 400:

```python
new_df = filter_by_treshold(df, 400, 0.9)
new_df.describe(percentiles=[0.25])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample_code</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>1498.000000</td>
      <td>1498.000000</td>
      <td>1498.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>9.390521</td>
      <td>24812.557410</td>
      <td>1.284007</td>
    </tr>
    <tr>
      <th>std</th>
      <td>5.498925</td>
      <td>14612.750864</td>
      <td>2.680880</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>6625.000000</td>
      <td>0.001188</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>5.000000</td>
      <td>8687.000000</td>
      <td>0.135721</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>10.000000</td>
      <td>34980.000000</td>
      <td>0.481585</td>
    </tr>
    <tr>
      <th>max</th>
      <td>18.000000</td>
      <td>40189.000000</td>
      <td>47.696011</td>
    </tr>
  </tbody>
</table>
</div>


We see even more filtering is happening; now, we have reduced the acceptable taxa to 1498

## Implement a way to call this function on a dataset with several thousand rows

The function that we have designed is already working on a thousand-row-plus database, but, if we wanted it to work on even bigger databases, so that it wont crash due to memory overload, we could try to partition the dataframe, not reading the whole DB at once. This would look like this:


```python
def filter_by_treshold_with_chunks(input_file, output_file, a, b):
    """
    A function that filers by a given *prevalence threshold*,
    calculating it using the mandatory parameters *a* (abundace-prevalence
    ratio) and *b* (minimum allowed prevalence), and using chunks to make
    it work with HUGE datasets, and storing the result on another file

    Args:
    input_file (str): a text string representing the file location of the
        comma-separated file that contains the info to filter
    output_file (str): a text string representing the location of the output file
    a (float): the parameter that relates abundance to prevalence
    b (float): the minimum allowed prevalence

    Returns:
        dict: a dictionary of the tax ids, with [number_of_samples, sum_of_percents]
            as values. Will be useful later, although maybe not just now
    """

    # We will not read the whole db at once, or pre-process, as its
    # not really necessary. Instead, we chunk-divide it
    chunks = pd.read_csv(input_file, sep=',', chunksize=1000)

    dictionary = {}; max_nb_samples = 0; exclude_lines = []
    for chunk in chunks:
        # For each chunk, we will calculate the exlusion parameters
        for taxon_id in chunk["taxonomy_id"].unique():
            sub_df = chunk[chunk["taxonomy_id"] == taxon_id]

            number_of_samples = len(sub_df["sample_code"].unique())
            sum_of_percents = sum(sub_df["percentage"])
            extracted_data = [number_of_samples, sum_of_percents]
            existing_data = dictionary.get(taxon_id, [0,0])

            # And store them in a dictionary, which we will then update
            # This is important, as some tax_ids might get cut between chunks
            new_data = [x + y for x, y in zip(extracted_data, existing_data)]
            dictionary[taxon_id] = new_data

            # We also calculate the max_nb_samples
            if new_data[0] > max_nb_samples: max_nb_samples = new_data[0]

    # With all the data calculated without loading anything in memory,
    # we apply the exclusion criteria to generate a list of unwanted tax_ids
    for tax_id, values in dictionary.items():
        number_of_samples = values[0]; sum_of_percents = values[1]
        percent_relative_abundace = sum_of_percents/number_of_samples
        mean_relative_abundace = percent_relative_abundace/100
        prevalence_treshold = (1 - a*mean_relative_abundace) + b
        prevalence = number_of_samples / max_nb_samples

        if prevalence_treshold >= 0 and prevalence_treshold > prevalence:
            exclude_lines.append(str(tax_id))

    # And we exclude the lines accordingly
    with open(input_file, 'r') as in_file, open(output_file, 'a') as out_file:
        for line in in_file:
            if line.split(",")[3] not in exclude_lines:
                out_file.write(line)

    return dictionary
```

Which we may now re-test, using the same values as above, to make sure this makes sense:


```python
# If the filtering is working ok, with a value of b = 0.25 (lax) and a = 400,
# we should get 2356 rows, which is a filtering of around 70%
filter_by_treshold_with_chunks("tech_exercise_abn.csv", "test_1.csv", 400, 0.25)
new_df = pd.read_csv("test_1.csv")
new_df.describe(percentiles=[0.25])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>sample_code</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>2356.000000</td>
      <td>2356.000000</td>
      <td>2356.000000</td>
      <td>2356.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>2548.757640</td>
      <td>9.462649</td>
      <td>24054.972411</td>
      <td>0.896903</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1530.760897</td>
      <td>5.541571</td>
      <td>14628.002089</td>
      <td>2.202330</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>6625.000000</td>
      <td>0.001188</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>1309.750000</td>
      <td>5.000000</td>
      <td>8687.000000</td>
      <td>0.093691</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>2567.500000</td>
      <td>10.000000</td>
      <td>34980.000000</td>
      <td>0.286815</td>
    </tr>
    <tr>
      <th>max</th>
      <td>6799.000000</td>
      <td>18.000000</td>
      <td>40189.000000</td>
      <td>47.696011</td>
    </tr>
  </tbody>
</table>
</div>

It works! Lets try with b = 0.9 (strict) and a = 400:

```python
filter_by_treshold_with_chunks("tech_exercise_abn.csv", "test_2.csv", 400, 0.9)
new_df = pd.read_csv("test_2.csv")
new_df.describe(percentiles=[0.25])
```

We get 1498 rows, as expected :p


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Unnamed: 0</th>
      <th>sample_code</th>
      <th>taxonomy_id</th>
      <th>percentage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>1498.000000</td>
      <td>1498.000000</td>
      <td>1498.000000</td>
      <td>1498.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>2482.951268</td>
      <td>9.390521</td>
      <td>24812.557410</td>
      <td>1.284007</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1497.065048</td>
      <td>5.498925</td>
      <td>14612.750864</td>
      <td>2.680880</td>
    </tr>
    <tr>
      <th>min</th>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>6625.000000</td>
      <td>0.001188</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>1284.250000</td>
      <td>5.000000</td>
      <td>8687.000000</td>
      <td>0.135721</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>2488.500000</td>
      <td>10.000000</td>
      <td>34980.000000</td>
      <td>0.481585</td>
    </tr>
    <tr>
      <th>max</th>
      <td>6709.000000</td>
      <td>18.000000</td>
      <td>40189.000000</td>
      <td>47.696011</td>
    </tr>
  </tbody>
</table>
</div>



## Document your implementation and write or suggest one or more unit tests

With regards to the documentation, I have written the docstrings using the sphinx specification, so that it could be auto-documented using said documentation tool. In the beginning, I thought about adding a README and a LICENSE file to the project, but, since we are using jupyter, I believe this should be accounted for already.

Thus, we can proceed with a sample unit test, which can be found below:


```python
import pandas as pd
import sys, os

class TestFilterByThreshold():

    def setUp():
        # First, we create a sample DataFrame
        # on which we will do the testing
        data = {
            'taxonomy_id': [6574, 9999, 5555, 2322, 3000],
            'sample_code': [11, 10, 19, 10, 1],
            'percentage': [0.01, 1.2, 3.1415, 0.0001, 2.71828]
        }
        # And we add it to the class variables
        TestFilterByThreshold.test_df = pd.DataFrame(data)

    def test_filter_by_threshold():
        # We then prepare some sample parameters
        a = 400; b = 0.25
        # And do the testing
        result_df = filter_by_treshold(TestFilterByThreshold.test_df, a, b)

        # We could, for example, assert that the number of columns will be three
        # (since filter_by_threshold removed the trailing first column if present)
        assert len(result_df.columns) == 3, ["Somehow, there seem to be a different",
                                             "number of cols than expected"]

        # We could also assert that the percentage column will have float values
        assert result_df["percentage"].dtypes == "float64", "Error: Percentage should be float"


TestFilterByThreshold.setUp()
TestFilterByThreshold.test_filter_by_threshold()
```

{{< alert type="info" >}}**Note:** I would like to mention that, despite having extensive experience with python coding, perhaps unit tests are the part of python that I have used the least. Thus, maybe this test is not the best, and I am sorry for that.{{< /alert >}}

## Explore the relationship between a and the number of filtered taxa:

### What are the largest and smallest possible values for a for this dataset?

As has previously been stated, our filter is _y = (1 - ax) + b_. In this equation, _y_ represents the fraction of samples (out of 1) where a taxon mustbe found, which means that it **must** take a value between 0 and 1. Thus, the value of a can be best expressed by the inequation:

$$ 0 \le (1 - ax) + b \le 1 $$

When solving this inequation, we would end up with:

$$  \frac{b}{x} \leq a \quad  \text{and} \quad a \leq \frac{1 + b}{x}  $$

In practical terms, we can calculate a in our dataset by using the different possible values for x and b:


```python
#We use the previous function to figure out the mean abundances (x).
# First, we extract all the calculated data:
dictionary = filter_by_treshold_with_chunks("tech_exercise_abn.csv", "test_3.csv", 400, 0.25)

all_mean_abundances = []; all_mean_frequencies = []; all_mean_ratios = []
# Then, we try to minimize the values that will give us the smaller a: b = 0 and x =?
for tax_id, values in dictionary.items():
    number_of_samples = values[0]; sum_of_percents = values[1]
    percent_relative_abundace = sum_of_percents/number_of_samples
    mean_relative_abundace = percent_relative_abundace/100
    all_mean_abundances.append(mean_relative_abundace)
    all_mean_frequencies.append(number_of_samples/19)
    abundance_to_prevalence = (number_of_samples/19) / mean_relative_abundace
    all_mean_ratios.append(abundance_to_prevalence)

min(all_mean_abundances), max(all_mean_abundances)
```




    (7.853392862051e-06, 0.11156720145223378)



We have already stated that the minimum value for _y_ will be 0. This value will be reached with a maximal value of _a$ only if _b_ is maximal, which means _b = 1_. This means that the biggest value for _a_ that makes sense in this dataset would be:

$$ y = (1 - a \cdot x) + b \quad \rightleftharpoons \quad 0 = 2 - a \cdot x  \quad \rightleftharpoons \quad a \cdot x = 2 \quad \rightleftharpoons \quad max_a = 2/min_x = 254339 $$

This makes sense; if we substitute \\(y = (1 - a \cdot x) + b\\) for \\(x = 7.853 \cdot 10^{-6}\\), _a = 254339_ and _b = 0_, we get y = 0

For the smallest value of a, we could use the inverse logic: _y = 0_ will only be reached with a minimal _a_ if _b = 0_, which means that the smaller value for _a_ that makes sense in this dataset would be:

$$ y = (1 - a \cdot x) + b \quad \rightleftharpoons \quad 0 = 1 - a \cdot x  \quad \rightleftharpoons \quad a \cdot x = 1 \quad \rightleftharpoons \quad min_a = 1/max_x = 8.968 $$

## How can we set the parameters if we want to filter 30% of taxa, if we set b to 0.2?

To solve this, we must first figure out where is the 30% treshold situated in terms of frequence (y), which is, after all, what we use to filter. Then, we can solve the x from there. Thus:


```python
import numpy as np
percentile_30 = np.percentile(all_mean_frequencies, 30)
percentile_30
```




    0.10526315789473684



Here, 30% of taxa will be excluded when we reach a frequency of 0.105, which, multiplied by 19, means when it is present on around 2 or more samples.

Thus, we can calculate the a treshold:

$$ 0.105 = (1 - a \cdot x) + 0.2 $$

from there,it follows:

$$ a = \frac{1.0948}{x}$$

to find the x corresponding to our selected y, we can scan the dictionary of values:


```python
this = []
for tax_id, values in dictionary.items():
    number_of_samples = values[0]; sum_of_percents = values[1]
    percent_relative_abundace = sum_of_percents/number_of_samples
    mean_relative_abundace = percent_relative_abundace/100
    if number_of_samples < 2:
        this.append(mean_relative_abundace)
1.0948/(sum(this)/len(this))
```




    1863.3179432978493




```python
dict2 = filter_by_treshold_with_chunks("tech_exercise_abn.csv", "test_4.csv", 1863, 0.2)
len(pd.read_csv("test_4.csv")["taxonomy_id"].unique())
```




    488



We can see that the number of filtered taxa is around 400, which is about 1/3 of the total (1324 taxa).


```python
len(pd.read_csv("tech_exercise_abn.csv")["taxonomy_id"].unique())
```




    1324


