---
title: "Nuevas Aventuras: Mi CV"
author: Pablo Marcos
date: 2023-02-15
output: pdf_document
menu:
  sidebar:
    name: CV LongTrec
    identifier: mi_cv_presentation_phd_longtrec
    parent: irb_turing_more
    weight: 40
---

Mi año de trabajo en el IARC terminó el 31 de Enero de 2022, pues en esta agencia sólo se hacen contratos de un año de duración; así, pese al orgullo que me supone haber participado de esta Agencia, especialmente cuando no suelen coger a gente tan joven como yo era o, directamente, sin doctorado, tuve que abandonar esta aventura y buscar un nuevo trabajo.

Por suerte, mi jefe, quien apreció enormemente mi trabajo en la Agencia, también buscaba un nuevo trabajo por esas fechas, cansado de las precarias condiciones que ofrecía la OMS; y, cuando fundó [Turing Biosystems](https://turing.bio/), su propia startup, no dudó en ofrecerme un puesto.

La verdad es que esta nueva experiencia con Adam, así como cuando estuve con él en el IARC, fue increíble; sencillamente, él es una bellísima persona, y el trabajo, que continuaba lo aprendido en el IARC, fue muy interesante.

En cualquier caso, lo cierto es que yo buscaba volver a España, y que el contrato que tenía, con un modesto sueldo de prácticas, no me permitía llegar de sobra a fin de més, por lo que comenzé a buscar una nueva aventura.

Así, el 15 de Febrero de 2023, preparé esta presentación, que ahora actualizo y publico, mostrando mi CV, cuando me presenté a una oferta de doctorados ofrecida por el [Consorcio LongTrec](https://longtrec.eu/). Aunque, al final, acabé decantándome por un trabajo como Research Assistant en el IRB Barcelona, creo que es útil tenerla a mano como CV interactivo y declaración de intenciones:

<div class="fluidMedia">
    <iframe src="/es/presentations/mi-cv" frameborder="0"> </iframe>
</div>


<style>

.fluidMedia {
    position: relative;
    padding-bottom: 56.25%; /* proportion value to aspect ratio 16:9 (9 / 16 = 0.5625 or 56.25%) */
    height: 0;
    overflow: hidden;
}

.fluidMedia iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

</style>
