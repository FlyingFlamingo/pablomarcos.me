---
title: "No estás solo"
date: 2024-11-30
menu:
  sidebar:
    name: Juntos
    identifier: juntos
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

No estás solo; es el mundo el que está solo.

No es tu culpa depender de una panda de locos, perdidos juguetes rotos que, imbuidos de odio, rompen a los otros.

A mi siempre me tendrás a tu lado. Siempre podrás contar con mi voz al despertar, con un beso antes de dormir y otro más para soñar, con mis ojos como estrellas para iluminar tu rostro.

Te amo, pero amar no es todo; mucho mas que casar, quisiera poderte llamar amigo.

---

📸 :  [Nong](https://unsplash.com/es/@californong) on [Unsplash](https://unsplash.com/es/fotos/figura-de-elefante-blanco-y-marron-3XisDwg6jAE); [Unsplash License](https://unsplash.com/es/licencia)

#juntos #together #poesiaenespañol #poesia #poetry #estrechosdemiras

{{<mastobutton>}}

