---
title: "Lucha por tí"
date: 2024-04-16
menu:
  sidebar:
    name:   Lucha por tí
    identifier: lucha
    parent: poemas
    weight: 40
author:
  name: Mr Framework
  image: /images/author/Mr Framework.png
---

Lucha por tí, y hazlo con orgullo, pues sabes que te va la vida en ello. Lucha como tu bien sabes, en la palestra, reponiéndote ante cada caída y cada lanzazo que te manda la vida.

Lucha por tí, porque te lo mereces, porque sueñas con ser feliz aguantando los embites de una hiedra de mil cabezas. Y lucha por  mí, que te quiero y nunca te dejaré sólo, que te acompañaré como Sancho Panza a tus molinos, caballeros andantes patrones de las causas perdidas.

Lucha sin violencia, por la paz, resistiendo como un gurú asceta los embites del destino. Recorre cien campos de sal, que empapen en su fatídico sabor a cada riachuelo de este valle de lágrimas; lucha como si fuera dulce y decoroso, no el morir, que ya no anhelas, sino el futuro en calma que te espera en casa.

Lucha una noche más contra el viento que nos separa, abre las alas y planea; a vista de pájaro, no tardarás mucho en encontrar mi nido.

---

📸 :  [GR Stocks](https://unsplash.com/es/@grstocks) on [Unsplash](https://unsplash.com/es/fotos/foto-en-escala-de-grises-de-una-persona-sosteniendo-un-vaso-Iq9SaJezkOE); [Unsplash License](https://unsplash.com/es/licencia)

#poesia #poetry #lucha #fight #poesiaenespañol

{{<mastobutton>}}
