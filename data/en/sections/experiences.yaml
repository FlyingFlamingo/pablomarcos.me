# section information
section:
  name: Experiences
  id: experiences
  enable: true
  weight: 3
  showOnNavbar: true
  # Can optionally hide the title in sections
  # hideTitle: true 
  
# Your experiences
experiences:
- company:
    name: "Biome Makers"
    url: "https://biomemakers.com/"
    location: Valladolid, Spain
    overview: Biome Makers is the leading Silicon-Valley startup centered around soil health and plant productivity engineering, working to ensure the Earth stays fertile and productive
  positions:
  - designation:
    start: Dec 2023
    responsibilities:
        - I work in the Data Science department, where I collaborate in the design and update of a system that **recommends actions** to improve soil health, quality and yield, reduce agricultural costs and point the way to soil regeneration. Using **python**, we interact with the **SQL databases** that make up [BeCrop Farm](https://biomemakers.com/es/tecnologia-becrop), the company's state-of-the-art precision agriculture system.
        - I have also performed multiple ad-hoc analyses to, among other things, expand the Recommender, using **GIS** to locate regions of interest, in a completely autonomous way. In addition, I have performed scientific validations of our results, working with the Marketing department to show the viability of our product.

- company:
    name: "Institut de Recerca Biomèdica"
    url: "https://www.irbbarcelona.org/"
    location: Barcelona, Spain
    overview: IRB is a private Research Foundation, which strives to develop advanced solutions for the medical needs of the future, specially with regards to cancer
  positions:
  - designation:
    start: May 2023
    end: Sept 2023
    responsibilities:
        - As part of the *Mechanisms of Disease* Branch, I have used a wide variety of tools (**PQSL**, **Python**, **Apache** and **HTML**) to maintain and update [the Chemical Checker](https://chemicalchecker.org/), a graph-based *bioactivity signaturizer*, as well as [Interactome3D](https://interactome3d.irbbarcelona.org), and interactive interactome viewer, and other of the group's websites.
- company:
    name: "Turing BioSystems"
    url: "https://turing.bio/"
    location: Lyon, France
  positions:
  - designation:
    start: February 2023
    end: May 2023
    responsibilities:
        - At Turing Bio, I worked in the intersection of Academia 🧪 and Enterprise 🏬 to elect, optimize, and design new therapies based on existing data. </br></br> Based on the work done at IARC, I transformed databases to provide as input to Turing's Interpretable AI Platform, which uses artificial intelligence to identify the origins of a disease like cancer, allowing pharmaceutical companies to develop drugs focused on the right target.
- company:
    name: "International Agency for Research on Cancer"
    url: "https://www.iarc.who.int/"
    location: Lyon, France
    # company overview
    overview: IARC is a United Nations / World Health Organization specialized Agency charged with conducting and coordinating research into the causes of cancer
  positions:
  - designation:
    start: February 2022
    end: January 2023
    responsibilities:
        - As part of the *OncoMetabolomics Team* of the *Nutrition and Metabolism* branch, I developed [CanGraph](https://flyingflamingo.codeberg.page/cangraph/), a python utility to study and analyse cancer-associated metabolites using knowledge graphs. It uses graph databases to find potential cancer associations in newly-discovered or already existing components of the human metabolome
- company:
    name: "European Engineering Learning Innovation and Science Alliance"
    url: "https://eelisa.eu/"
    location: European Union
    # company overview
    overview: EELISA is an alliance of leading European Technical Universities meant to define and implement a common model of "European engineer" rooted in society.
  positions:
  - designation:
    start: September 2021
    end: January 2022
    responsibilities:
        - I worked as part of the SSERIES (*Science for a Sustainable Envision of Reality and Information for an Engaged Society*) EELISA Protocommunity, where I [helped present the project](https://blogs.upm.es/sseries-eelisa/) to other Member Institutions and define the reach of the projects, its activities, and its online presence. I also helped set up a beautiful website for their [Art & Science](https://blogs.upm.es/artandscience/) competition.
- company:
    name: "Centre Hospitalier Universitaire Grenoble-Alpes"
    url: "https://www.chu-grenoble.fr/"
    location: Grenoble, France
    # company overview
    overview: CHU Grenoble is a leading medical center in Grenoble, which itself is one of France's research capitals
  positions:
  - designation: 
    start: April 2021
    end: July 2021
    responsibilities: 
        - As part of my ERASMUS, I did an Internship at CHU Grenoble, which I used as the basis for my Degree's Thesis. [I worked on the effects of tacrolimus on patients of renal transplants](https://codeberg.org/FlyingFlamingo/destination-grenoble), using Python and biostatistical analysis.
- company:
    name: "Universidad Politécnica de Madrid"
    url: "https://www.caminos.upm.es/default.asp"
    location: Madrid, Spain
    # company overview
    overview: UPM is one of the of the leading European technical universities and the most prestigious engineer's school in the Madrid Region.
  positions:
  - designation: Intern at the Department of Applied Mathematics
    start: February 2021
    end: March 2021
    responsibilities: 
        - In January 2021, I was rehired to redesign the website for the [Educational Innovation Group "Mathematical Thinking"'s scientific journal](https://revista.giepm.com/), which I helped transition to wordpress
  - designation: Intern at the Educational Innovation Program
    start: June 2020
    end: September 2020
    # don't provide end date if you are currently working there. It will be replaced by "Present"
    # end: Dec 2020
    # give some points about what was your responsibilities at the company.
    responsibilities: 
        - During the 'From Game to Theory' teacher-student colaboration scholarship, I programmed ['Who's that Function'](https://www.flyingflamingo.itch.io/whos-that-function), an interactive game which teaches students the basics of functions and their properties in an engaging and interesting manner, using a dynamic similar to the 'Guess Who?' series." The game received an Honourable Mention in the ["Science in Action" contest](https://cienciaenaccion.org/resolucion-jurado-ciencia-en-accion-xxii-no-presencial-y-adopta-una-estrella/).
